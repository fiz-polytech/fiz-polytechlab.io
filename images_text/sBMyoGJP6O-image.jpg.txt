В, По прямому проводу течет постоянный ток, однородно распределенный по сечению. Объемная плотность энергии магнитного поля одинакова
на расстояниях г, = 6,2 см ит — 9,1 см от оси провода.

( не» | Определите радиус В провода, если он находится в вакууме, а магнитная проницаемость вещества провода равна д: — 7,0.

  

Ответ: 12.217 м | ем
