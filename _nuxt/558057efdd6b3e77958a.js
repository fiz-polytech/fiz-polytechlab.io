(window.webpackJsonp = window.webpackJsonp || []).push([
    [3], {
        1161: function(t, e, r) {
            var content = r(1167);
            "string" == typeof content && (content = [
                [t.i, content, ""]
            ]), content.locals && (t.exports = content.locals);
            (0, r(101).default)("2da12190", content, !0, {
                sourceMap: !1
            })
        },
        1166: function(t, e, r) {
            "use strict";
            var n = r(1161);
            r.n(n).a
        },
        1167: function(t, e, r) {
            (e = r(100)(!1)).push([t.i, ".index[data-v-33760314]{height:83vh}.index-text[data-v-33760314]{font-size:1.2em}.index-contacts[data-v-33760314]{margin-top:20px}", ""]), t.exports = e
        },
        1177: function(t, e, r) {
            "use strict";
            r.r(e);
            var n = {
                    layout: "polytech",
                    data: function() {
                        return {
                            gutter: 40
                        }
                    }
                },
                l = (r(1166), r(34)),
                component = Object(l.a)(n, (function() {
                    var t = this,
                        e = t.$createElement,
                        r = t._self._c || e;
                    return r("a-card", {
                        staticClass: "index",
                        attrs: {
                            title: "Сайт с ответами на курсы Политеха. Копия от Кирилла."
                        }
                    }, [r("section", {
                        staticClass: "index-text"
                    }, [t._v("\n    Так как сайт был переработан целиком, могут наблюдаться баги. "), r("br"), t._v(" "), r("b", [t._v("Большая просьба")]), t._v(" по найденым проблемам обращаться по нижеприведённым контактам: "), r("br")]), t._v(" "), r("section", {
                        staticClass: "index-contacts"
                    }, [r("a-row", {
                        attrs: {
                            gutter: t.gutter
                        }
                    }, [r("a-col", {
                        attrs: {
                            span: 12,
                            xs: 8,
                            sm: 12,
                            md: 12,
                            lg: 8,
                            xl: 4
                        }
                    }, [r("h3", [t._v("Telegram")])]), t._v(" "), r("a-col", {
                        attrs: {
                            span: 12,
                            xs: 8,
                            sm: 12,
                            md: 12,
                            lg: 8,
                            xl: 4
                        }
                    }, [r("a", {
                        attrs: {
                            href: "https://t-do.ru/rellowyy"
                        }
                    }, [t._v("@rellowyy")])])], 1), t._v(" "), r("a-row", {
                        attrs: {
                            gutter: t.gutter
                        }
                    }, [r("a-col", {
                        attrs: {
                            span: 12,
                            xs: 8,
                            sm: 12,
                            md: 12,
                            lg: 8,
                            xl: 4
                        }
                    }, [r("h3", [t._v("Email")])]), t._v(" "), r("a-col", {
                        attrs: {
                            span: 12,
                            xs: 8,
                            sm: 12,
                            md: 12,
                            lg: 8,
                            xl: 4
                        }
                    }, [r("a", {
                        attrs: {
                            href: "mailto:andrew@rellowy.tech"
                        }
                    }, [t._v("andrew@rellowy.tech")])])], 1)], 1)])
                }), [], !1, null, "33760314", null);
            e.default = component.exports
        }
    }
]);
