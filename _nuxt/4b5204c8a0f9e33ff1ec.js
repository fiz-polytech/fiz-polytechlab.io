(window.webpackJsonp = window.webpackJsonp || []).push([
    [2], {
        1162: function(t, e, r) {
            var content = r(1172);
            "string" == typeof content && (content = [
                [t.i, content, ""]
            ]), content.locals && (t.exports = content.locals);
            (0, r(101).default)("47874bd2", content, !0, {
                sourceMap: !1
            })
        },
        1163: function(t, e, r) {
            var content = r(1174);
            "string" == typeof content && (content = [
                [t.i, content, ""]
            ]), content.locals && (t.exports = content.locals);
            (0, r(101).default)("69d3ef7c", content, !0, {
                sourceMap: !1
            })
        },
        1168: function(t, e, r) {
            "use strict";
            var n = r(21),
                o = r(68),
                l = r(79),
                c = r(568),
                f = r(181),
                m = r(42),
                d = r(127).f,
                _ = r(182).f,
                v = r(36).f,
                h = r(1169).trim,
                k = n.Number,
                w = k,
                y = k.prototype,
                x = "Number" == l(r(264)(y)),
                P = "trim" in String.prototype,
                O = function(t) {
                    var e = f(t, !1);
                    if ("string" == typeof e && e.length > 2) {
                        var r, n, o, l = (e = P ? e.trim() : h(e, 3)).charCodeAt(0);
                        if (43 === l || 45 === l) {
                            if (88 === (r = e.charCodeAt(2)) || 120 === r) return NaN
                        } else if (48 === l) {
                            switch (e.charCodeAt(1)) {
                                case 66:
                                case 98:
                                    n = 2, o = 49;
                                    break;
                                case 79:
                                case 111:
                                    n = 8, o = 55;
                                    break;
                                default:
                                    return +e
                            }
                            for (var code, c = e.slice(2), i = 0, m = c.length; i < m; i++)
                                if ((code = c.charCodeAt(i)) < 48 || code > o) return NaN;
                            return parseInt(c, n)
                        }
                    }
                    return +e
                };
            if (!k(" 0o1") || !k("0b1") || k("+0x1")) {
                k = function(t) {
                    var e = arguments.length < 1 ? 0 : t,
                        r = this;
                    return r instanceof k && (x ? m((function() {
                        y.valueOf.call(r)
                    })) : "Number" != l(r)) ? c(new w(O(e)), r, k) : O(e)
                };
                for (var j, N = r(31) ? d(w) : "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(","), C = 0; N.length > C; C++) o(w, j = N[C]) && !o(k, j) && v(k, j, _(w, j));
                k.prototype = y, y.constructor = k, r(43)(n, "Number", k)
            }
        },
        1169: function(t, e, r) {
            var n = r(26),
                o = r(80),
                l = r(42),
                c = r(1170),
                f = "[" + c + "]",
                m = RegExp("^" + f + f + "*"),
                d = RegExp(f + f + "*$"),
                _ = function(t, e, r) {
                    var o = {},
                        f = l((function() {
                            return !!c[t]() || "​" != "​" [t]()
                        })),
                        m = o[t] = f ? e(v) : c[t];
                    r && (o[r] = m), n(n.P + n.F * f, "String", o)
                },
                v = _.trim = function(t, e) {
                    return t = String(o(t)), 1 & e && (t = t.replace(m, "")), 2 & e && (t = t.replace(d, "")), t
                };
            t.exports = _
        },
        1170: function(t, e) {
            t.exports = "\t\n\v\f\r   ᠎             　\u2028\u2029\ufeff"
        },
        1171: function(t, e, r) {
            "use strict";
            var n = r(1162);
            r.n(n).a
        },
        1172: function(t, e, r) {
            (e = r(100)(!1)).push([t.i, '.ant-input-number[data-v-319b5f04]{width:100%}.answer[data-v-319b5f04]{margin-bottom:20px}.answer-params__input[data-v-319b5f04]{display:grid;grid-template-columns:50px 1fr 150px;grid-column-gap:20px;margin-bottom:10px}.answer-params__label[data-v-319b5f04]{display:flex;align-items:center;justify-content:center}.answer-params__result[data-v-319b5f04]{margin-top:20px;padding:8px 15px;background-color:#f6ffed;border:1px solid #b7eb8f;color:rgba(0,0,0,.65);font-size:14px;font-variant:tabular-nums;line-height:1.5;list-style:none;font-feature-settings:"tnum","tnum","tnum";position:relative;border-radius:4px}', ""]), t.exports = e
        },
        1173: function(t, e, r) {
            "use strict";
            var n = r(1163);
            r.n(n).a
        },
        1174: function(t, e, r) {
            (e = r(100)(!1)).push([t.i, ".type-card[data-v-9df5247e]{width:100%;height:-webkit-max-content;height:-moz-max-content;height:max-content}.type-card-pagination[data-v-9df5247e]{width:100%;display:flex;align-items:center;justify-content:center}", ""]), t.exports = e
        },
        1175: function(t, e, r) {
            "use strict";
            r.r(e);
            r(179), r(51), r(52), r(35), r(126);
            var n = r(557),
                o = r(24),
                l = r(120),
                c = (r(265), r(180), r(67), r(1168), r(69), r(70), r(567), function(t) {
                    var e = 0,
                        r = !1,
                        a = t;
                    if (0 === a) return "0";
                    for (; e < 10 && !1 === r;) a = (+t).toFixed(e), Math.abs(1 * +a / +t - 1) <= .0025 && (r = !0), e = +e + 1;
                    return a.toString()
                }),
                f = {
                    props: {
                        question: {
                            type: Object,
                            default: function() {
                                return {}
                            }
                        },
                        type: {
                            type: Number,
                            default: 0
                        }
                    },
                    data: function() {
                        return {
                            url: "/",
                            task: this.question,
                            answer: null
                        }
                    },
                    computed: {},
                    methods: {
                        getReplacedFormulaWithParams: function(t, e) {
                            var form = t;
                            for (var param in e) {
                                var r = e[param],
                                    n = r.name,
                                    o = String(r.defaultValue).replace(",", "."),
                                    l = "[".concat(n, "]"),
                                    c = +o < 0 ? "(".concat(o, ")") : o;
                                form = form.split(l).join(c)
                            }
                            return form
                        },
                        getAnswer: function() {
                            if (this.task.solution_text) this.answer = this.task.solution_text;
                            else {
                                var t = this.task,
                                    e = t.solution,
                                    r = t.params,
                                    n = t.dimension;
                                try {
                                    var o = this.getReplacedFormulaWithParams(e, r);
                                    if (4 !== this.type) {
                                        var l = "function(){var result = (".concat(o, "); return result;}"),
                                            f = window.eval("(".concat(l, ")"))(),
                                            m = c(f);
                                        this.answer = n ? "".concat(m, " ").concat(n) : m
                                    } else {
                                        var d = "function(){var result = (".concat(o, "); return result;}"),
                                            _ = window.eval("(".concat(d, ")"))();
                                        this.answer = _
                                    }
                                } catch (t) {
                                    this.answer = ""
                                }
                            }
                        },
                        download: function(t) {
                            this.$api.get(t, {
                                responseType: "blob"
                            }).then((function(t) {
                                var data = t.data,
                                    e = window.URL.createObjectURL(new Blob([data])),
                                    link = document.createElement("a");
                                link.href = e, link.setAttribute("download", "exam.pdf"), document.body.appendChild(link), link.click()
                            }))
                        }
                    }
                },
                m = (r(1171), r(34));

            function d(object, t) {
                var e = Object.keys(object);
                if (Object.getOwnPropertySymbols) {
                    var r = Object.getOwnPropertySymbols(object);
                    t && (r = r.filter((function(t) {
                        return Object.getOwnPropertyDescriptor(object, t).enumerable
                    }))), e.push.apply(e, r)
                }
                return e
            }
            var _ = {
                    layout: "polytech",
                    components: {
                        AnswerItem: Object(m.a)(f, (function() {
                            var t = this,
                                e = t.$createElement,
                                r = t._self._c || e;

				/*
                    Удаляет подраздел из пути картинки, подгружая их всегда из пути /images.
                    Пример:
                    Было: /images/XmgVeGhO4N/HbzUX10WXZ-result.jpg
                    Стало: /images/HbzUX10WXZ-result.jpg
                */
                
                // Тестирует подгрузку картинки
                function test_load_image(src,error){
                    var tester = new Image();
                    tester.onload = function() {
                        //console.log()
                    };
                    tester.onerror = function() {
                       console.log("https://polytech.rellowy.tech/" + error)
                    };
                    tester.src=src;
                
                }
                /*
				let image_old = t.task.image
				let solution_image_old =  t.task.solution_image
				console.log("task.image:" + image_old)
				console.log("task.solution_image:" + solution_image_old)
                */
                if(t.task.solution_image !== null){
                    var tsi = t.task.solution_image.split('/')
                    //console.log("tsi:" + tsi)
                    if(tsi.length==4){
                        t.task.solution_image = tsi[1] + "/" + tsi[3]
                        //test_load_image(t.task.solution_image, solution_image_old)
                    }
                }
				if(t.task.image !== null){
                    var ti = t.task.image.split('/')
                    //console.log("ti:" + ti)
                    if(ti.length==4){
                        t.task.image = ti[1] + "/" + ti[3]
                        //test_load_image(t.task.image, image_old)
                    }
                }
				
                
                            return t.task ? r("a-card", {
                                staticClass: "answer"
                            }, [1 === t.type ? [t.task.solution_image && null !== t.task.solution_image ? r("img", {
                                attrs: {
                                    slot: "cover",
                                    alt: "Ответ",
                                    src: "" + t.url + t.task.solution_image
                                },
                                slot: "cover"
                            }) : [r("img", {
                                attrs: {
                                    slot: "cover",
                                    alt: "Ответ",
                                    src: "" + t.url + t.task.image
                                },
                                slot: "cover"
                            }), t._v(" "), null !== t.task.solution_text ? r("a-card-meta", {
                                attrs: {
                                    title: t.task.solution_text
                                }
                            }) : t._e()]] : t._e(), t._v(" "), 2 === t.type ? [t.task.image ? r("img", {
                                attrs: {
                                    slot: "cover",
                                    alt: "Ответ",
                                    src: "" + t.url + t.task.image
                                },
                                slot: "cover"
                            }) : t._e(), t._v(" "), t.task.params ? [t._l(t.task.params, (function(param, i) {
                                return r("div", {
                                    key: i,
                                    staticClass: "answer-params__input"
                                }, [r("div", {
                                    staticClass: "answer-params__label"
                                }, [r("katex-element", {
                                    attrs: {
                                        expression: param.tex
                                    }
                                })], 1), t._v(" "), r("a-input", {
                                    model: {
                                        value: param.defaultValue,
                                        callback: function(e) {
                                            t.$set(param, "defaultValue", e)
                                        },
                                        expression: "param.defaultValue"
                                    }
                                }), t._v(" "), r("div", {
                                    staticClass: "answer-params__label"
                                }, [r("katex-element", {
                                    attrs: {
                                        expression: param.dimension
                                    }
                                })], 1)], 1)
                            })), t._v(" "), r("a-button", {
                                attrs: {
                                    type: "primary"
                                },
                                on: {
                                    click: t.getAnswer
                                }
                            }, [t._v("\n        Получить ответ\n      ")]), t._v(" "), t.answer ? r("div", {
                                staticClass: "answer-params__result"
                            }, [r("katex-element", {
                                attrs: {
                                    expression: t.answer
                                }
                            })], 1) : t._e()] : t._e()] : t._e(), t._v(" "), 3 === t.type ? [null !== t.task.solution_text ? r("img", {
                                attrs: {
                                    slot: "cover",
                                    alt: "Вопрос",
                                    src: "" + t.url + t.task.image
                                },
                                slot: "cover"
                            }) : t._e(), t._v(" "), null !== t.task.solution_image ? r("img", {
                                attrs: {
                                    slot: "cover",
                                    alt: "Ответ",
                                    src: "" + t.url + t.task.solution_image
                                },
                                slot: "cover"
                            }) : t._e(), t._v(" "), null !== t.task.solution_text ? r("a-card-meta", {
                                attrs: {
                                    title: t.task.solution_text
                                }
                            }) : t._e()] : t._e(), t._v(" "), 4 === t.type ? [r("img", {
                                attrs: {
                                    src: "" + t.url + t.task.image,
                                    alt: "Картинка с вопросами"
                                }
                            }), t._v(" "), t._l(t.task.params, (function(param, i) {
                                return r("div", {
                                    key: i,
                                    staticClass: "answer-params__input"
                                }, [r("div", {
                                    staticClass: "answer-params__label"
                                }, [r("katex-element", {
                                    attrs: {
                                        expression: param.tex
                                    }
                                })], 1), t._v(" "), r("a-input", {
                                    model: {
                                        value: param.defaultValue,
                                        callback: function(e) {
                                            t.$set(param, "defaultValue", e)
                                        },
                                        expression: "param.defaultValue"
                                    }
                                }), t._v(" "), r("div", {
                                    staticClass: "answer-params__label"
                                }, [r("katex-element", {
                                    attrs: {
                                        expression: param.dimension
                                    }
                                })], 1)], 1)
                            })), t._v(" "), r("a-button", {
                                attrs: {
                                    type: "primary"
                                },
                                on: {
                                    click: t.getAnswer
                                }
                            }, [t._v("\n      Получить ответ\n    ")]), t._v(" "), t.answer ? r("div", {
                                staticClass: "answer-params__result"
                            }, [r("katex-element", {
                                attrs: {
                                    expression: t.answer
                                }
                            })], 1) : t._e()] : t._e(), t._v(" "), 5 === t.type ? [t._l(t.task.image.split(","), (function(img, i) {
                                return r("img", {
                                    key: "img-" + i,
                                    attrs: {
                                        src: "" + t.url + img,
                                        alt: t.task.name
                                    }
                                })
                            })), t._v(" "), r("div", {
                                staticClass: "answer-params__result",
                                staticStyle: {
                                    "white-space": "pre-line"
                                }
                            }, [t._v("\n      " + t._s(t.task.solution_text) + "\n    ")])] : t._e(), t._v(" "), 7 === t.type ? [r("h2", [t._v(t._s(t.task.name))]), t._v(" "), r("a-button", {
                                attrs: {
                                    type: "primary"
                                },
                                on: {
                                    click: function(e) {
                                        return t.download("" + t.url + t.task.solution_image)
                                    }
                                }
                            }, [t._v("\n      Скачать PDF со списком вопросов\n    ")]), t._v(" "), r("div", {
                                staticClass: "answer-params__result",
                                staticStyle: {
                                    "white-space": "pre-line"
                                }
                            }, [t._v("\n      " + t._s(t.task.solution_text) + "\n    ")])] : t._e()], 2) : t._e()
                        }), [], !1, null, "319b5f04", null).exports
                    },
                    data: function() {
                        return {
                            tasks: [],
                            meta: null,
                            currentPage: 1,
                            tasksPerPage: [],
                            pageSize: 5
                        }
                    },
                    computed: function(t) {
                        for (var i = 1; i < arguments.length; i++) {
                            var source = null != arguments[i] ? arguments[i] : {};
                            i % 2 ? d(Object(source), !0).forEach((function(e) {
                                Object(o.a)(t, e, source[e])
                            })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(source)) : d(Object(source)).forEach((function(e) {
                                Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(source, e))
                            }))
                        }
                        return t
                    }({}, Object(l.b)({
                        getTasks: "polytech/getTasks"
                    })),
                    created: function() {
                        var t = this.$route.params,
                            e = t.subject,
                            r = t.sem,
                            section = t.section,
                            n = t.type,
                            o = this.getTasks(e, +r, +section, +n),
                            meta = o.meta,
                            l = o.tasks;
                        this.tasks = l, this.meta = meta
                    },
                    mounted: function() {
                        this.onChangePage(this.currentPage, this.pageSize)
                    },
                    methods: {
                        getTitle: function() {
                            var t = this.meta,
                                e = t.subject_name,
                                r = t.type_name,
                                section = t.section;
                            return "".concat(e, ", раздел ").concat(section, ": ").concat(r)
                        },
                        onChangePage: function(t, e) {
                            this.currentPage = t;
                            var r = Object(n.a)(this.tasks),
                                o = this.currentPage * this.pageSize - this.pageSize,
                                l = this.currentPage * this.pageSize;
                            this.tasksPerPage = r.slice(o, l), window.scrollTo(0, 0)
                        }
                    },
                    head: function() {
                        return {
                            title: this.getTitle()
                        }
                    }
                },
                v = (r(1173), Object(m.a)(_, (function() {
                    var t = this,
                        e = t.$createElement,
                        r = t._self._c || e;
                    return r("a-card", {
                        key: t.currentPage,
                        staticClass: "type-card",
                        attrs: {
                            title: t.getTitle
                        }
                    }, [r("div", {
                        staticClass: "type-card-scroll"
                    }, t._l(t.tasksPerPage, (function(e, i) {
                        return r("answer-item", {
                            key: i,
                            attrs: {
                                question: e,
                                type: t.meta.type_id
                            }
                        })
                    })), 1), t._v(" "), r("div", {
                        staticClass: "type-card-pagination"
                    }, [r("a-pagination", {
                        attrs: {
                            total: t.tasks.length,
                            "show-less-items": "",
                            "page-size": t.pageSize
                        },
                        on: {
                            change: t.onChangePage
                        },
                        model: {
                            value: t.currentPage,
                            callback: function(e) {
                                t.currentPage = e
                            },
                            expression: "currentPage"
                        }
                    })], 1)])
                }), [], !1, null, "9df5247e", null));
            e.default = v.exports
        }
    }
]);
