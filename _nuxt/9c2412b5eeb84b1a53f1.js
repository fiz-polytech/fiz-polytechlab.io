(window.webpackJsonp = window.webpackJsonp || []).push([
    [4], {
        1160: function(t, e, n) {
            var content = n(1165);
            "string" == typeof content && (content = [
                [t.i, content, ""]
            ]), content.locals && (t.exports = content.locals);
            (0, n(101).default)("0310739c", content, !0, {
                sourceMap: !1
            })
        },
        1164: function(t, e, n) {
            "use strict";
            var r = n(1160);
            n.n(r).a
        },
        1165: function(t, e, n) {
            (e = n(100)(!1)).push([t.i, ".ant-table-fixed-header .ant-table-scroll .ant-table-header{margin-bottom:-20px!important;padding-bottom:20px!important;overflow:hidden!important}.ant-table-fixed-header>.ant-table-content>.ant-table-scroll>.ant-table-body{max-height:calc(60vh - 80px)!important;height:100%}.schedule{height:83vh}", ""]), t.exports = e
        },
        1176: function(t, e, n) {
            "use strict";
            n.r(e);
            n(179), n(51), n(67), n(52), n(35), n(126);
            var r = n(24),
                c = n(120);

            function o(object, t) {
                var e = Object.keys(object);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(object);
                    t && (n = n.filter((function(t) {
                        return Object.getOwnPropertyDescriptor(object, t).enumerable
                    }))), e.push.apply(e, n)
                }
                return e
            }
            var l = {
                    layout: "polytech",
                    data: function() {
                        return {
                            columns: [{
                                title: "Предмет",
                                dataIndex: "subject",
                                key: "subject",
                                width: "15%"
                            }, {
                                title: "Раздел",
                                dataIndex: "section",
                                key: "section",
                                width: "17%"
                            }, {
                                title: "Начало",
                                dataIndex: "start",
                                key: "start",
                                width: "15%"
                            }, {
                                title: "Конец",
                                dataIndex: "end",
                                key: "end",
                                width: "15%"
                            }, {
                                title: "Штрафы",
                                dataIndex: "penalty",
                                key: "penalty",
                                width: "20%"
                            }, {
                                title: "Тест",
                                dataIndex: "moduleTest",
                                key: "moduleTest",
                                width: "15%"
                            }]
                        }
                    },
                    computed: function(t) {
                        for (var i = 1; i < arguments.length; i++) {
                            var source = null != arguments[i] ? arguments[i] : {};
                            i % 2 ? o(Object(source), !0).forEach((function(e) {
                                Object(r.a)(t, e, source[e])
                            })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(source)) : o(Object(source)).forEach((function(e) {
                                Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(source, e))
                            }))
                        }
                        return t
                    }({}, Object(c.c)("polytech", ["schedule"]), {
                        data: function() {
                            var t = this,
                                data = Object.keys(this.schedule).map((function(e, i) {
                                    var n = t.schedule[e].map((function(t, e) {
                                        var n = t.start,
                                            r = t.end,
                                            c = t.name,
                                            o = t.penaltyStart,
                                            l = t.penaltyEnd,
                                            d = t.moduleTest;
                                        return {
                                            key: "".concat(i).concat(e),
                                            start: n,
                                            end: r,
                                            section: c,
                                            penalty: !!o && !!l && "".concat(o, " — ").concat(l),
                                            moduleTest: !1 !== d ? d : ""
                                        }
                                    }));
                                    return {
                                        key: i,
                                        subject: e,
                                        children: n
                                    }
                                }));
                            return data
                        }
                    }),
                    head: function() {
                        return {
                            title: "Расписание"
                        }
                    }
                },
                d = (n(1164), n(34)),
                component = Object(d.a)(l, (function() {
                    var t = this.$createElement,
                        e = this._self._c || t;
                    return e("a-card", {
                        staticClass: "schedule",
                        attrs: {
                            title: "Расписание курсов на 2020 год"
                        }
                    }, [e("a-table", {
                        attrs: {
                            columns: this.columns,
                            "data-source": this.data,
                            scroll: {
                                x: 1e3,
                                y: 240
                            }
                        }
                    })], 1)
                }), [], !1, null, null, null);
            e.default = component.exports
        }
    }
]);