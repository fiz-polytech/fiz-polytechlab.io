#!/bin/bash

FILES=("_nuxt" "favicon.ico")

for i in {1..20}; do
	for j in {1..20};do
		for k in {1..20}; do
			for f in ${FILES[@]}; do
				mkdir -p ./$i/$j/$k;
				ln -s "../../../../$f" ./$i/$j/$k/;
				ln -s "../../../$f" ./$i/$j/;
				ln -s "../../$f" ./$i/;
			done
		done
	done

done


